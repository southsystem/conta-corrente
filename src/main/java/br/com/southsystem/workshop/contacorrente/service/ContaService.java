package br.com.southsystem.workshop.contacorrente.service;

import br.com.southsystem.workshop.contacorrente.broker.BrokerOutput;
import br.com.southsystem.workshop.contacorrente.model.Conta;
import br.com.southsystem.workshop.contacorrente.model.enumeration.TipoContaEnum;
import br.com.southsystem.workshop.contacorrente.repository.ContaRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ContaService {
    private final CpfRemoteService cpfRemoteService;
    private final ContaRepository contaRepository;
    private final BrokerOutput brokerOutput;


    public ContaService(CpfRemoteService cpfRemoteService, ContaRepository contaRepository, BrokerOutput brokerOutput) {
        this.cpfRemoteService = cpfRemoteService;
        this.contaRepository = contaRepository;
        this.brokerOutput = brokerOutput;
    }

    public Conta insert(Conta conta) {
        cpfRemoteService.findNomeByCpf(conta.getCpf())
            .ifPresent(result -> conta.setNome(result.get("name")));

        final Conta result = contaRepository.save(conta);

        brokerOutput.contaCorrenteCriada()
            .send(MessageBuilder.withPayload(result).build());

        return result;
    }

    public Optional<Conta> getOne(String id) {
        return Optional.ofNullable(contaRepository.findOne(id));
    }

    public Page<Conta> getAll(Pageable pageable) {
        return contaRepository.findAll(pageable);
    }

    public Page<Conta> getByTipoConta(TipoContaEnum tipoContaEnum, Pageable pageable) {
        return contaRepository.findByTipoConta(tipoContaEnum, pageable);
    }

    public void delete(String id) {
        contaRepository.delete(id);
    }

    public Conta save(String id, Conta conta) {
        Optional<Conta> dbConta = getOne(id);

        if (dbConta.isPresent()) {
            conta.setId(id);
            return contaRepository.save(conta);
        } else {
            throw new IllegalArgumentException("Conta inválida");
        }
    }
}
