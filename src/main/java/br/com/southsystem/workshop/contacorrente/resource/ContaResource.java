package br.com.southsystem.workshop.contacorrente.resource;

import br.com.southsystem.workshop.contacorrente.model.Conta;
import br.com.southsystem.workshop.contacorrente.model.enumeration.TipoContaEnum;
import br.com.southsystem.workshop.contacorrente.service.ContaService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/conta")
public class ContaResource {

    private final ContaService contaService;

    public ContaResource(ContaService contaService) {
        this.contaService = contaService;
    }

    @PostMapping
    public ResponseEntity<Conta> insert(@Valid @RequestBody Conta conta) {
        return ResponseEntity.ok(contaService.insert(conta));
    }

    @GetMapping
    public ResponseEntity<Page<Conta>> getAll(Pageable pageable) {
        return ResponseEntity.ok(contaService.getAll(pageable));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Conta> get(@PathVariable("id") String id) {
        return contaService.getOne(id)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity
                        .notFound().build());
    }

    @GetMapping("/tipo-conta/{tipoConta}")
    public ResponseEntity<Page<Conta>> getTipoConta(@PathVariable("tipoConta") TipoContaEnum tipoContaEnum
            , Pageable pageable) {
        return ResponseEntity.ok(contaService.getByTipoConta(tipoContaEnum, pageable));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") String id) {
        contaService.delete(id);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/{id}")
    public ResponseEntity<Conta> update(@PathVariable("id") String id, @Valid @RequestBody Conta conta) {
        return ResponseEntity.ok(contaService.save(id, conta));
    }















}
